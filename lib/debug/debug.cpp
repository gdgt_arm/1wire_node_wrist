#include "debug.h"

void dumpToPin(uint8_t data, uint8_t pin) {
    BIT_CLEAR(DB_DUMP_PORT, pin);
    NOP;
    NOP;

    BIT_SET(DB_DUMP_PORT, pin);
    if (data & 0x80) {
        NOP;
        NOP;
        NOP;
        NOP;
    }
    BIT_CLEAR(DB_DUMP_PORT, pin);

    BIT_SET(DB_DUMP_PORT, pin);
    if (data & 0x40) {
        NOP;
        NOP;
        NOP;
        NOP;
    }
    BIT_CLEAR(DB_DUMP_PORT, pin);

    BIT_SET(DB_DUMP_PORT, pin);
    if (data & 0x20) {
        NOP;
        NOP;
        NOP;
        NOP;
    }
    BIT_CLEAR(DB_DUMP_PORT, pin);

    BIT_SET(DB_DUMP_PORT, pin);
    if (data & 0x10) {
        NOP;
        NOP;
        NOP;
        NOP;
    }
    BIT_CLEAR(DB_DUMP_PORT, pin);

    BIT_SET(DB_DUMP_PORT, pin);
    if (data & 0x08) {
        NOP;
        NOP;
        NOP;
        NOP;
    }
    BIT_CLEAR(DB_DUMP_PORT, pin);

    BIT_SET(DB_DUMP_PORT, pin);
    if (data & 0x04) {
        NOP;
        NOP;
        NOP;
        NOP;
    }
    BIT_CLEAR(DB_DUMP_PORT, pin);

    BIT_SET(DB_DUMP_PORT, pin);
    if (data & 0x02) {
        NOP;
        NOP;
        NOP;
        NOP;
    }
    BIT_CLEAR(DB_DUMP_PORT, pin);

    BIT_SET(DB_DUMP_PORT, pin);
    if (data & 0x01) {
        NOP;
        NOP;
        NOP;
        NOP;
    }
    BIT_CLEAR(DB_DUMP_PORT, pin);
}
