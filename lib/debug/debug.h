#ifndef DEBUG_H
#define DEBUG_H

#include <Arduino.h>
#include "bit_ops.h"

#define DB_DUMP_PORT PORTB

// #define DEBUG
#ifdef DEBUG
#define DB_DUMP_TO_PIN(data, pin) dumpToPin(data, pin)
#define DB_BIT_SET(value, bit) BIT_SET(value, bit)
#define DB_BIT_CLEAR(value, bit) BIT_CLEAR(value, bit)
#define DB_PULSE(value, bit) \
    BIT_SET(value, bit);     \
    BIT_CLEAR(value, bit)
#else
#define DB_DUMP_TO_PIN(data, pin)
#define DB_BIT_SET(value, bit)
#define DB_BIT_CLEAR(value, bit)
#define DB_PULSE(value, bit)
#endif

#ifndef NOP
#define NOP __asm__ __volatile__("nop\n\t")
#endif

void dumpToPin(uint8_t data, uint8_t pin);

#endif
