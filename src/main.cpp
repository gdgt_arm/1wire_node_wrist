// #pragma GCC optimize ("-O0")
#include "tiny1wireX5.h"
#include <Arduino.h>
// #include <EEPROM.h>
#include "neo.c"
#include "1wire_node_commands.h"

//                +--\/--+
// (RESET/dW) PB5 | 1  8 | VCC
//       (OW) PB3 | 2  7 | PB2 (SCK/SCL/SOL)
//     (HALL) PB4 | 3  6 | PB1 (MISO/CC)
//            GND | 4  5 | PB0 (MOSI/SDA/NEO/LED)
//                +------+

// Arduino ISP
//           MISO - 1  2 - VCC
//            SCK - 3  4 - MOSI
//          Reset - 5  6 - GND

#define LED_ON sendPixel(0, 0, 32)
#define LED_OFF sendPixel(0, 0, 0)

#define HALL_PIN 4
#define SOLENOID_PIN 2
#define CC_PIN 1
#define NEO_PIN 0

#define RXNEO 0x14

#define NEO_FOLLOW_NOTHING 0
#define NEO_FOLLOW_CC 1
#define NEO_FOLLOW_HALL 2

void byteRecd(uint8_t state, uint8_t* buf);

uint8_t romCode[] = { 0xDD, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }; // Family code: DD
uint8_t data[3];
uint8_t neoFollowing = NEO_FOLLOW_NOTHING;
uint8_t prev;

void reportHallState();

void setup()
{
    OSCCAL = 0x6A; // User calibration

    // DDxn: 1 = output, 0 = input
    DDRB = _BV(SOLENOID_PIN) | _BV(NEO_PIN); // | _BV(0) | _BV(1) | _BV(2) | _BV(4);
    // PORTxn: 1 = output high/pull-up enabled, 0 = output low/pull-up disabled
    // PORTB = 0x03;
    PORTB = _BV(CC_PIN); // | _BV(HALL_PIN); // CC_PIN is input with pull-up enabled

    DB_BIT_SET(PORTB, 4);

    owSetup(romCode, byteRecd);

    sei();

    DB_BIT_CLEAR(PORTB, 4);
}

void loop()
{
    uint8_t pin;

    owTick();

    if (neoFollowing == NEO_FOLLOW_CC) {
        pin = BIT_READ(PINB, CC_PIN); // Active low
        if (pin != prev) {
            if (pin) {
                sendPixel(0, 0, 0);
            } else { // Active low
                sendPixel(0, 32, 0);
            }
            prev = pin;
        }
    } else if (neoFollowing == NEO_FOLLOW_HALL) {
        pin = BIT_READ(PINB, HALL_PIN); // Active low
        if (pin != prev) {
            if (pin) {
                sendPixel(0, 0, 0);
            } else { // Active low
                sendPixel(32, 0, 0);
            }
            prev = pin;
        }
    }
}

/*
Wrist:

3 3 NEO stop following coupler confirm state
  6 NEO follow coupler confirm state
  9 Get Coupler Confirm state

5 3 NEO stop following Hall state
  6 NEO follow Hall state
  9 Report Hall state - through trip and release

9 3 Solenoid OFF
  6 Solenoid ON

Common:

C 3 LED OFF
  6 LED ON
  A Set NEO - 3 bytes
*/

void byteRecd(uint8_t state, uint8_t* buf)
{
    if (state == RXDEVCMD) {
        switch (buf[0]) {
        case OW_WRIST_NEO_CC_STOP: // NEO stop following coupler confirm state
            neoFollowing = NEO_FOLLOW_NOTHING;
            owRxBytes(1, RXDEVCMD);
            break;
        case OW_WRIST_NEO_CC_START: // NEO follow coupler confirm state
            neoFollowing = NEO_FOLLOW_CC;
            prev = 0xFF;
            owRxBytes(1, RXDEVCMD);
            break;
        case OW_WRIST_GET_CC: // Get Coupler Confirm
            data[0] = BIT_READ(PINB, CC_PIN) ? 0 : 1;
            data[1] = data[0] ? 0x5E : 0;
            // Send value
            owTxBytes(data, 2);
            break;
        case OW_WRIST_NEO_HALL_STOP: // NEO stop following Hall state
            neoFollowing = NEO_FOLLOW_NOTHING;
            owRxBytes(1, RXDEVCMD);
            break;
        case OW_WRIST_NEO_HALL_START: // NEO follow Hall state
            neoFollowing = NEO_FOLLOW_HALL;
            prev = 0xFF;
            owRxBytes(1, RXDEVCMD);
            break;
        case OW_WRIST_REPORT_HALL: // Report Hall state
            reportHallState();
            owRxBytes(1, RXDEVCMD);
            break;
        case OW_WRIST_SOL_OFF: // Solenoid off
            BIT_CLEAR(PORTB, SOLENOID_PIN);
            sendPixel(0, 0, 0);
            owRxBytes(1, RXDEVCMD);
            break;
        case OW_WRIST_SOL_ON: // Solenoid on
            BIT_SET(PORTB, SOLENOID_PIN);
            sendPixel(0, 0, 32);
            owRxBytes(1, RXDEVCMD);
            break;
        case OW_LED_OFF: // turn off LED
            LED_OFF;
            DB_BIT_CLEAR(PORTB, 4);
            owRxBytes(1, RXDEVCMD);
            break;
        case OW_LED_ON: // turn on LED
            LED_ON;
            DB_BIT_SET(PORTB, 4);
            owRxBytes(1, RXDEVCMD);
            break;
        case OW_NEO_SET: // Set NEO
            owRxBytes(4, RXNEO);
            break;
        default:
            state = IDLE;
        }
    } else if (state == RXNEO) { // Set NEO color
        sendPixel(buf[0], buf[1], buf[2]);
        owRxBytes(1, RXDEVCMD);
    }
}

void reportHallState()
{
    // TODO Add timeout

    // Hall is active low
    sendPixel(0, 0, 0);
    while (BIT_READ(PINB, HALL_PIN)) { } // Waiting for Hall to trip
    owAssert();
    sendPixel(32, 0, 0);
    while (!BIT_READ(PINB, HALL_PIN)) { } // Waiting for Hall to release
    owRelease();
    sendPixel(0, 0, 0);
}